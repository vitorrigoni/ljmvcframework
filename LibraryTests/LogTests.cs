﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using LJMVCFramework.Lib.Core;
using System.Text;

namespace LibraryTests
{
    [TestClass]
    public class LogTests
    {
        [TestMethod]
        public void Testa_Escreve_No_Arquivo_De_Log()
        {
            var f = Log.LogFilePath + "Alert.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Alert", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Write(LogLevel.Alert, "Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Varias_Linhas()
        {
            var template = string.Format(Log.Template, DateTime.Now, "Debug", "Linha");
            var sb = new StringBuilder();

            var f = Log.LogFilePath + "Debug.log";
            File.Delete(f);

            for (int i = 0; i < 4; i++)
            {
                sb.AppendLine(template);
                Log.Debug("Linha");
            }

            var r = File.ReadAllText(f);
            Assert.AreEqual(sb.ToString(), r);
        }

        [TestMethod]
        public void Testa_Escreve_Cem_Linhas()
        {
            for (int i = 0; i < 100; i++)
            {
                Log.Debug("Linha");
            }
            Assert.AreEqual(true, File.Exists(Log.LogFilePath + "Debug.log"));
        }

        [TestMethod]
        public void Testa_Escreve_Emergency()
        {
            var f = Log.LogFilePath + "Emergency.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Emergency", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Emergency("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Alert()
        {
            var f = Log.LogFilePath + "Alert.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Alert", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Alert("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Critical()
        {
            var f = Log.LogFilePath + "Critical.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Critical", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Critical("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Error()
        {
            var f = Log.LogFilePath + "Error.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Error", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Error("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Warning()
        {
            var f = Log.LogFilePath + "Warning.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Warning", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Warning("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Notice()
        {
            var f = Log.LogFilePath + "Notice.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Notice", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Notice("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Info()
        {
            var f = Log.LogFilePath + "Info.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Info", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Info("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }

        [TestMethod]
        public void Testa_Escreve_Debug()
        {
            var f = Log.LogFilePath + "Debug.log";
            var expected = string.Format(Log.Template, DateTime.Now, "Debug", "Teste" + Environment.NewLine);
            if (File.Exists(f))
                File.Delete(f);

            Log.Debug("Teste");
            var r = File.ReadAllText(f);

            Assert.AreEqual(expected, r);
        }
    }
}
