﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LJMVCFramework.Lib.Network;
using System.Net;
using System.Net.Mail;

namespace LibraryTests
{
    [TestClass]
    public class EmailConfigTests
    {
        [TestMethod]
        public void Testa_Le_Configuracoes_Corretamente()
        {
            var expected = new EmailConfig()
            {
                Host = "localhost",
                Port = 25,
                Credentials = new NetworkCredential("UserName", "Password"),
                From = new MailAddress("example@localhost.com.br", "Example User")
            };

            var config = new EmailConfig();
            config.ReadConfig("Default");

            Assert.AreEqual(expected.Host, config.Host);
        }

        [TestMethod, ExpectedException(typeof(Exception))]
        public void Testa_Nao_Existe_Configuracao()
        {
            var config = new EmailConfig();
            config.ReadConfig("Configuacao Inexistente");
        }
    }
}
