﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LJMVCFramework.Lib.Utility;

namespace LibraryTests
{
    [TestClass]
    public class InflectorTests
    {
        [TestMethod]
        public void Testa_Camelizado_Com_String_Normatizada()
        {
            var expected = "TextoCamelizadoCorretamente";
            var input = "texto_camelizado_corretamente";

            var result = Inflector.Camelize(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Camelizado_Com_String_Zoada()
        {
            var expected = "TextoCom123EEspaços";
            var input = "TEXTO COm 123 _ e Espaços";

            var result = Inflector.Camelize(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Humanizado_Somente_Com_Lower_Strings_Separadas_Por_Underscore()
        {
            var expected = "Texto Humanizado Corretamente";
            var input = "Texto_HUMANIZADO_CORRetamente";

            var result = Inflector.Humanize(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Humanizado_Com_Numeros_E_Espacos()
        {
            var expected = "Texto Com 123 E Espaços";
            var input = "TEXTO COm 123 _ e Espaços";

            var result = Inflector.Humanize(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Humanizado_Com_Palavras_de_Uma_Letra()
        {
            var expected = "T E X T O C O M P A Lav Ra S";
            var input = "t e x t o  c o m p a lav   ra s";

            var result = Inflector.Humanize(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Remover_Acentos()
        {
            var expected = "Texto com varios acentos de ccc a n vai pegar";
            var input = "Texto com vários acentos de ççç â ñ vai pegár";

            var result = Inflector.ConvertAccents(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Slug_Com_Caracteres_Especiais()
        {
            var expected = "asdas-aaa-eee";
            var input = "%$¨*!%$ as%%da&s ããã ééé &&";

            var result = Inflector.Slug(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Slug_Com_String_Normatizada_Sem_Acentos()
        {
            var expected = "texto-cadastrado";
            var input = "Texto Cadastrado";

            var result = Inflector.Slug(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Slug_Com_Acentos()
        {
            var expected = "texto-cadastrado";
            var input = "Téxtó Cãdâstrádô";

            var result = Inflector.Slug(input);

            Assert.AreEqual(expected, result);
        }
        
    }
}
