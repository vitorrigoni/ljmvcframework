﻿using LJMVCFramework.Lib.Application;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryTests.Views.TestCase
{
    public partial class Add : Form, IView
    {
        public dynamic ViewBag { get; set; }
        public string Slug { get; set; }
        public BindingSource BS { get; set; }
        public AppController OwnerController { get; set; }

        public Add()
        {
            InitializeComponent();
        }
    }
}
