﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LJMVCFramework.Lib.Network;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Collections.Generic;

namespace LibraryTests
{
    [TestClass]
    public class EmailTests
    {
        [TestMethod]
        public void Testa_Cria_Email_Configurado()
        {
            var expected = new Email();
            expected.Config = new EmailConfig()
            {
                Host = "localhost"
            };

            var result = new Email("Default");

            Assert.AreEqual(expected.Config.Host, result.Config.Host);
        }

        [TestMethod]
        public void Testa_Cria_Email_Sem_Configuracao()
        {
            var result = new Email();

            Assert.AreEqual(null, result.Config.Host);
        }

        [TestMethod]
        public void Testa_Cria_Email_Com_Configuracao_Externa_Pronta()
        {
            var conf = new EmailConfig()
            {
                Host = "testmailserver.test.com",
                From = new MailAddress("test@example.com.br", "Test Example"),
                Port = 25,
                Credentials = new NetworkCredential("TestUser", "TestPassword")
            };

            var e = new Email(conf);

            Assert.AreEqual(conf.Host, e.Config.Host);
        }

        [TestMethod]
        public void Testa_Leitura_Do_Template()
        {
            var expected = @"
<body>
    <p>Olá, Vitor!</p>
    <p>Neste e-mail estamos demonstrando como utilizar os templates</p>
    <p>Nossa data atual é: 16/10/2015</p>
</body>
";
            var e = new Email();
            e.ViewBag["NomeUsuario"] = "Vitor";
            e.ViewBag["DataHoraAtual"] = "16/10/2015";

            e.WriteMailFromTemplate();

            Assert.AreEqual(expected, e.Body);
        }

        [TestMethod, ExpectedException(typeof(FileNotFoundException))]
        public void Testa_Template_Nao_Encontrado()
        {
            var e = new Email();
            e.WriteMailFromTemplate("NaoExiste");
        }

        [TestMethod]
        public void Testa_Inserir_Dados_No_Template()
        {
            var expected = "Olá, Usuário! Este é um exemplo do que podemos fazer com templates de e-mail!";
            var template = "Olá, {{Usuario}}! {{Mensagem}}";
            var e = new Email();
            e.ViewBag["Usuario"] = "Usuário";
            e.ViewBag["Mensagem"] = "Este é um exemplo do que podemos fazer com templates de e-mail!";

            e.WriteMailFromTemplateString(template);

            Assert.AreEqual(expected, e.Body);
        }

        [TestMethod]
        public void Testa_Inserir_Dados_Que_Nao_Possuem_Tags()
        {
            var expected = "Este email possui {{QuantidadeLinhas}} linhas";
            var template = "Este email possui {{QuantidadeLinhas}} linhas";

            var e = new Email();
            // Não defino a tag QuantidadeLinhas na ViewBag

            e.WriteMailFromTemplateString(template);

            Assert.AreEqual(expected, e.Body);
        }

        [TestMethod]
        public void Testa_Validacao_De_Email_Com_Sucesso()
        {
            var input = "helpdesk@brasigran.com.br";

            Assert.AreEqual(true, Email.Validate(input));
        }

        [TestMethod]
        public void Testa_Email_Invalido()
        {
            var input = "asda@  lll ;";
            Assert.AreEqual(false, Email.Validate(input));
        }

        [TestMethod]
        public void Testa_Valida_Lista_Com_Emails_Validos()
        {
            var emails = new List<string>()
            {
                "asd@asd.com",
                "aaa@aaa.com.br",
                "vitor@vitor.com.ru"
            };

            Assert.AreEqual(true, Email.Validate(emails));
        }

        [TestMethod]
        public void Testa_Valida_Lista_Com_Um_Email_Invalido()
        {
            var emails = new List<string>()
            {
                "asd@asd.com",
                "aaa@aaa.com.br",
                "vitor@vitor.com.ru",
                "aa  .. @asda.com"
            };

            Assert.AreEqual(false, Email.Validate(emails));
        }
        
        [TestMethod]
        public void Testa_Envia_Debug()
        {
            var email = new Email()
            {
                Body = "Corpo do email Teste",
                Subject = "Assunto Teste",
                Debug = true,
                From = new MailAddress("test@example.com.br")
            };
            email.To.Add("example@example.com.br");
            

            Assert.AreEqual(true, email.Send());
        }

        [TestMethod]
        public void Testa_Insere_ViewBag_Data_Com_Template_Vazio()
        {
            var expected = string.Empty;

            var e = new Email();

            var result = e.InsertViewBagData("");

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Testa_Envia_Com_Email_Invalido()
        {
            var e = new Email("Default");
            e.To.Add(new MailAddress("as adad @ aaa"));

            Assert.AreEqual(false, e.Send());
        }

        [TestMethod]
        [ExpectedException(typeof(SmtpException))] // Estou esperando uma SmtpException pois a configuração Default não possui um host válido para envio de e-mail
        public void Testa_Envia_Corretamente()
        {
            var e = new Email("Default");
            e.To.Add(new MailAddress("example@sample.com"));
            e.Send();
        }
    }
}
