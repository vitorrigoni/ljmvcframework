﻿using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LJMVCFramework.Lib.Application;

namespace LibraryTests
{
    [TestClass]
    public class ControllerTests
    {
        class TestCaseController : AppController
        {
            public void Add()
            {
                Render();
            }

            public void Add(int id)
            {

            }

            public void ViewlessMethod()
            {
                Render();
            }

            public void DifferentView()
            {
                var v = new LibraryTests.Views.TestCase.List();
                Render(v);
            }
        }

        [TestMethod]
        public void Testa_Render_Carrega_View_Correta()
        {
            var c = new TestCaseController();
            c.Add();

            Assert.AreEqual("Add", c.View.Text);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Testa_View_Nao_Existe()
        {
            var c = new TestCaseController();
            c.ViewlessMethod();
        }

        [TestMethod]
        public void Testa_Render_View_Diferente()
        {
            var c = new TestCaseController();
            c.DifferentView();

            Assert.AreEqual("List", c.View.Text);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Testa_Invoke_Metodo_inexistente()
        {
            var c = new TestCaseController();
            c.Invoke("InexistenteMetodo");
        }

        [TestMethod]
        public void Testa_Invoke_Corretamente()
        {
            var c = new TestCaseController();
            c.Invoke("Add");

            Assert.AreEqual("Add", c.View.Text);
        }
    }
}
