# README #

* LJMVCFramework is a small framework simulating the MVC Environment for developing Windows Forms Applications.
* The idea behind this structure is to be able to test as much code as possible, creating clean conventions, controllers that are responsible for doing stuff and completely unaware views
* It provides mostly the structure for Controllers and Views, and understands that you'll be using your own Models (you can use tools like Entity Framework or create your own data access tools)
* It also provides utility static classes such as an Inflector class for dealing with strings
* Documentation is included in a separate project, along with tests for the Library
* Version: 0.0.1

### How do I get set up? ###

* There is no special configuration for setting up. You can:
* 1. Import the single LJMVCFramework.Lib.dll as a reference to your project and start using right away
* 2. Import the project LJMVCFramework.Lib into your project and modify it for your own needs
* Open the solution in Visual Studio 2012+
* Compile a Release build
* Have Fun =)

### Contribution guidelines ###

* Writing tests: Tests shall be written only on the LibraryTests project. Please be aware that test cases are always camel-cased and underscore separated. E.g.: This_Is_A_Sample_Test_Case
* Tests must also be written in en_US OR pt_BR
* Code convention follows .Net Framework conventions (https://msdn.microsoft.com/en-us/library/ms229002(v=vs.110).aspx)

### Who do I talk to? ###

* Feel free to contact me at vitor@lemonjuicewebapps.com