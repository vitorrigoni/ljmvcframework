﻿/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using LJMVCFramework.Lib.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace LJMVCFramework.Lib.Network
{

    /// <summary>
    /// Classe encapsulando a MailMessage facilitando o processo de envio de e-mails
    /// </summary>
    public class Email : MailMessage
    {
        /// <summary>
        /// Regex para o padrão de e-mail
        /// </summary>
        public static string EmailPattern { get { return @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"; } }
        
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, object> ViewBag { get; set; }
        
        /// <summary>
        /// Configuração para o envio do e-mail
        /// </summary>
        public EmailConfig Config { get; set; }
     
        /// <summary>
        /// Caso Debug esteja true, o e-mail não será enviado e será escrito no debug.log
        /// </summary>
        public bool Debug { get; set; }

        /// <summary>
        /// Construtor sem configuração
        /// </summary>
        public Email()
        {
            Config = new EmailConfig();
            ViewBag = new Dictionary<string, object>();
        }

        /// <summary>
        /// Aceita o nome de uma configuração para buscar no arquivo EmailConfig.xml
        /// </summary>
        /// <param name="ConfigName"></param>
        public Email(string ConfigName)
        {
            Config = new EmailConfig(ConfigName);
            ViewBag = new Dictionary<string, object>();
        }

        /// <summary>
        /// Aceita uma configuração pronta
        /// </summary>
        /// <param name="config"></param>
        public Email(EmailConfig config)
        {
            Config = config;
            ViewBag = new Dictionary<string, object>();
        }

        /// <summary>
        /// Lê um template de e-mail substituindo as tags {{exemplo}} pelos valores
        /// passados na ViewBag
        /// Ex: ViewBag["Usuario"] = Vitor;
        /// No template deverá existir uma tag {{Usuario}} que será substituída pelo valor contido na ViewBag
        /// </summary>
        /// <param name="Template"></param>
        public void WriteMailFromTemplate(string Template = "default")
        {
            var template = File.ReadAllText(Path.GetFullPath(string.Format(@"Templates/Email/{0}.html", Template)));

            template = InsertViewBagData(template);

            IsBodyHtml = true;
            Body = template;
        }

        /// <summary>
        /// Escreve o corpo do e-mail a partir de uma string.
        /// Pode ser utilizado para passar a string manualmente ou
        /// utilizar um cadastro de templates de e-mail a partir
        /// do banco de dados, por exemplo
        /// </summary>
        /// <param name="TemplateString"></param>
        public void WriteMailFromTemplateString(string TemplateString = "")
        {
            TemplateString = InsertViewBagData(TemplateString);
            Body = TemplateString;
        }

        /// <summary>
        /// Insere os dados da ViewBag na string de template passada.
        /// </summary>
        /// <param name="Template"></param>
        /// <returns></returns>
        public string InsertViewBagData(string Template)
        {
            if (string.IsNullOrEmpty(Template)) return string.Empty;

            foreach (var item in ViewBag)
            {
                Template = Template.Replace("{{" + item.Key.ToString() + "}}", item.Value.ToString());
            }
            return Template;
        }

        /// <summary>
        /// Realiza o envio do e-mail usando a configuração definida
        /// </summary>
        /// <returns></returns>
        public bool Send()
        {
            From = Config.From == null ? From : Config.From;
            if (!Validate(To.Select(t => t.Address)))
            {
                return false;
            }

            if (!Debug)
            {
                var client = new SmtpClient()
                {
                    Host = Config.Host,
                    Port = Config.Port | 25,
                    Credentials = Config.Credentials
                };
                client.Send(this);
            }
            else
            {
                var debugEmail = string.Format(@"
Debug de E-mail:
From: {0} <{1}>
To: {2}
Body:
{3}
", this.From.DisplayName,
   this.From.Address,
   string.Join(", ", this.To.Select(t => string.Format("{0} <{1}>", t.DisplayName, t.Address))),
   this.Body);

                Log.Debug(debugEmail);
            }

            return true;
        }

        /// <summary>
        /// Valida se o endereço de e-mail possui formato correto
        /// </summary>
        /// <param name="Address">Endereço a ser validado</param>
        /// <returns></returns>
        public static bool Validate(string Address)
        {
            var r = new Regex(Email.EmailPattern);
            if (!r.IsMatch(Address))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Valida uma lista de endereços de e-mail
        /// </summary>
        /// <param name="Addresses">Qualquer lista de strings que implemente IEnumerable</param>
        /// <returns></returns>
        public static bool Validate(IEnumerable<string> Addresses)
        {
            foreach (var email in Addresses)
            {
                if (!Validate(email))
                {
                    return false;
                }
            }
            return true;
        }

        

    }
}

