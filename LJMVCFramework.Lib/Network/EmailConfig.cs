﻿/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using LJMVCFramework.Lib.Core;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace LJMVCFramework.Lib.Network
{

    /// <summary>
    /// Esta configuração será lida a partir de um arquivo de configuração
    /// responsável por armazenar as informações e retornar a configuração
    /// desejada.
    /// </summary>
    public class EmailConfig
    {
        public MailAddress From { get; set; }
        public string Host { get; set; }
        public NetworkCredential Credentials { get; set; }
        public int Port { get; set; }

        /// <summary>
        /// Construtor sem parâmetros caso deseje implementar uma configuração
        /// em runtime
        /// </summary>
        public EmailConfig()
        {

        }

        /// <summary>
        /// Chama o método ReadConfig e popula toda a configuração de acordo com
        /// o parâmetro Name
        /// </summary>
        /// <param name="Name"></param>
        public EmailConfig(string Name)
        {
            ReadConfig(Name);
        }

        /// <summary>
        /// Irá buscar o arquivo Email.config e interpretar o xml
        /// de configurações buscando a configuração com o nome
        /// passado no parâmetro Name
        /// </summary>
        /// <param name="Name"></param>
        public void ReadConfig(string Name)
        {
            try
            {
                var path = Path.GetFullPath("Config/Emailconfig.xml");
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(path, XmlReadMode.InferTypedSchema);

                var config = dataSet.Tables["add"].Rows.OfType<DataRow>().FirstOrDefault(r => r["name"].ToString() == Name);
                if (config == null)
                    throw new Exception("Configuração Inválida");

                this.Host = config["host"].ToString();
                this.Port = Convert.ToInt32(config["port"]);
                this.Credentials = new NetworkCredential(config["username"].ToString(), config["password"].ToString());
                this.From = new MailAddress(config["address"].ToString(), config["displayName"].ToString());
            }
            catch (Exception ex)
            {
                Log.Error("Não foi possível criar a configuração selecionada: " + ex.Message);
                throw ex;
            }
        }
    }
}