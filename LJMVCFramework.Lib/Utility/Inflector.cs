﻿/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LJMVCFramework.Lib.Utility
{
    public static class Inflector
    {
        /// <summary>
        /// Returns the given lower_case_and_underscored_word as a CamelCased word.
        /// </summary>
        /// <param name="LowerCaseAndUnderscoreWord"></param>
        /// <returns>Camelized word. LikeThis.</returns>
        public static string Camelize(string LowerCaseAndUnderscoreWord)
        {
            var input = Inflector.Humanize(LowerCaseAndUnderscoreWord);
            return input.Replace(" ", string.Empty);
        }

        /// <summary>
        /// Returns the given underscored_word_group as a Human Readable Word Group.
        /// (Underscores are replaced by spaces and capitalized following words.)
        /// </summary>
        /// <param name="LowerCaseAndUnderscoredWord"></param>
        /// <returns>Human-readable string</returns>
        public static string Humanize(string LowerCaseAndUnderscoredWord)
        {
            var words = LowerCaseAndUnderscoredWord.ToLower().Replace("_", " ").Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            for (int i = 0; i < words.Length; i++)
                words[i] = words[i][0].ToString().Trim().ToUpper() + words[i].Substring(1).Trim();

            return string.Join(" ", words);
        }

        /// <summary>
        /// Returns a string with all spaces converted to hiphens (by default), accented
        /// characters converted to non-accented characters, and non word characters removed.
        /// </summary>
        /// <param name="Input">The string you want to slug</param>
        /// <param name="Replacement"></param>
        /// <returns></returns>
        public static string Slug(string Input, string Replacement = "-")
        {
            var result = Regex.Replace(Input.ToLower(), @"[^\w\s]+", "").Trim();
            result = Inflector.ConvertAccents(result.Replace(" ", Replacement));
            return result;
        }

        /// <summary>
        /// Converts accents into non-accented characters
        /// </summary>
        /// <param name="Input">The string you want to remove characters from</param>
        /// <returns></returns>
        public static string ConvertAccents(string Input)
        {
            var normalizedString = Input.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
