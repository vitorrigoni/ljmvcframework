﻿/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LJMVCFramework.Lib.Core
{
    /// <summary>
    /// Available log levels according to RFC 3164
    /// https://www.ietf.org/rfc/rfc3164.txt
    /// </summary>
	public enum LogLevel
	{
        /// <summary>
        /// Emergency: system is unusable
        /// </summary>
		Emergency = 0,
        
        /// <summary>
        /// Alert: action must be taken immediately
        /// </summary>
		Alert = 1,

        /// <summary>
        /// Critical: critical conditions
        /// </summary>
        Critical = 2,
        
        /// <summary>
        /// Error: error conditions
        /// </summary>
		Error = 3,
		
        /// <summary>
        /// Warning: warning conditions
        /// </summary>
        Warning = 4,
		
        /// <summary>
        /// Notice: normal but significant condition
        /// </summary>
        Notice = 5,
		
        /// <summary>
        /// Informational: informational messages
        /// </summary>
        Info = 6,
		
        /// <summary>
        /// Debug: debug-level messages
        /// </summary>
        Debug = 7,
	}
}
