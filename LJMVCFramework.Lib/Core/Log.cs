﻿/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LJMVCFramework.Lib.Core
{
    /// <summary>
    /// Logging class
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// Path for writing logs
        /// </summary>
        public static string LogFilePath = Path.GetFullPath("Logs/");

        /// <summary>
        /// Log line template.
        /// Example: 16/10/2015 11:35:54 - Debug: This is a Debug message
        /// </summary>
        public static string Template = @"{0:dd/MM/yyyy hh:mm:ss} - {1}: {2}";

        /// <summary>
        /// Writes a log message with a specific level to a log file
        /// </summary>
        /// <param name="lvl">Log level</param>
        /// <param name="Message">Message to be written</param>
        public static void Write(LogLevel lvl, string Message)
        {
            var path = String.Format("{0}{1}.log", LogFilePath, lvl);
            var msg = string.Format(Template, DateTime.Now, lvl, Message);
            var sw = File.AppendText(path);
            sw.WriteLine(msg);
            sw.Close();
        }
        
        /// <summary>
        /// Convenience method for log level Emergency
        /// </summary>
        /// <param name="Message"></param>
        public static void Emergency(string Message)
        {
            Write(LogLevel.Emergency, Message);
        }

        /// <summary>
        /// Convenience method for log level Alert
        /// </summary>
        /// <param name="Message"></param>
        public static void Alert(string Message)
        {
            Write(LogLevel.Alert, Message);
        }

        /// <summary>
        /// Convenience method for log level Critical
        /// </summary>
        /// <param name="Message"></param>
        public static void Critical(string Message)
        {
            Write(LogLevel.Critical, Message);
        }

        /// <summary>
        /// Convenience method for log level Error
        /// </summary>
        /// <param name="Message"></param>
        public static void Error(string Message)
        {
            Write(LogLevel.Error, Message);
        }

        /// <summary>
        /// Convenience method for log level Warning
        /// </summary>
        /// <param name="Message"></param>
        public static void Warning(string Message)
        {
            Write(LogLevel.Warning, Message);
        }

        /// <summary>
        /// Convenience method for log level Notice
        /// </summary>
        /// <param name="Message"></param>
        public static void Notice(string Message)
        {
            Write(LogLevel.Notice, Message);
        }

        /// <summary>
        /// Convenience method for log level Info
        /// </summary>
        /// <param name="Message"></param>
        public static void Info(string Message)
        {
            Write(LogLevel.Info, Message);
        }

        /// <summary>
        /// Convenience method for log level Debug
        /// </summary>
        /// <param name="Message"></param>
        public static void Debug(string Message)
        {
            Write(LogLevel.Debug, Message);
        }

    }
}
