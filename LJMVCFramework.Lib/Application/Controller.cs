﻿/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using LJMVCFramework.Lib.Utility;

namespace LJMVCFramework.Lib.Application
{
    /// <summary>
    /// Base class for 
    /// </summary>
    public class Controller
    {
        /// <summary>
        /// Controller Name without the word Controller
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Full type name of the view being invoked
        /// </summary>
        public string ViewFullName { get; set; }

        /// <summary>
        /// List of all available views inside the calling namespace
        /// </summary>
        public List<Type> ViewTypesList { get; set; }

        /// <summary>
        /// The view being rendered
        /// </summary>
        public Form View { get; set; }

        /// <summary>
        /// Default controller constructor
        /// Calls BeforeLoad() immediately
        /// </summary>
        public Controller()
        {
            Name = this.GetType().Name.Replace("Controller", "");

            ViewTypesList = (from t in this.GetType().Assembly.GetTypes()
                             where t.IsClass
                             where t.Namespace.Contains("Views")
                             select t).ToList();

            BeforeLoad();
        }

        /// <summary>
        /// Invokes a method and instantiates a view with the same name of the method
        /// being invoked. This method does not render the view. You still need to call Render()
        /// 
        /// I'll always instantiate the parameterless method.
        /// 
        /// Use this method when instantiating forms, so you'll have the view instantiated before having called Render.
        /// This allows you to prepare data, bind stuff and anything you'd rather do before rendering a view.
        /// View.InitializeComponent() is already called and designer-created members are already available
        /// (as long as you've set their modifier to public, of course haha)
        /// 
        /// After finishing instantiating the view, invokes the requested method and calls AfterLoad()
        /// </summary>
        /// <param name="MethodName">Method to be invoked. Chains to the view to be rendered</param>
        public void Invoke(string MethodName)
        {
            var method = this.GetType().GetMethod(MethodName, Type.EmptyTypes);
            if (method == null)
                throw new InvalidOperationException(string.Format("Requested method does not exist. Make sure you have created method {0} on Controller {1}Controller", MethodName, Name));

            this.ViewFullName = string.Format("{0}.{1}", Name, MethodName);
            var invokedView = ViewTypesList.FirstOrDefault(t => t.AssemblyQualifiedName.Contains(ViewFullName));
            if (invokedView == null)
                throw new InvalidOperationException("The invoked view does not exist. Make sure you have created the form " + ViewFullName);

            var instance = (Form)Activator.CreateInstance(invokedView);
            this.View = instance;
            ((IView)View).Slug = Inflector.Slug(MethodName);
            ((IView)View).Name = Inflector.Camelize(MethodName);
            ((IView)View).OwnerController = (AppController)this;

            AfterLoad();

            method.Invoke(this, null);
        }

        /// <summary>
        /// Method for rendering views.
        /// If no view is passed, it'll try to find a view under the namespace of the controller (without the word Controller)
        /// plus the method being called.
        /// 
        /// E.g.: Calling UsersController.NewUser() will search for a Form inside the folder "Views/Users/" with the name of NewUser
        /// If it doesn't exist, throws an InvalidOperationException
        /// 
        /// </summary>
        /// <param name="RenderView">Windows Form that implements the IView Interface</param>
        public void Render(IView RenderView = null, [CallerMemberName] string Method = "")
        {
            BeforeRender();
            if (RenderView == null)
            {
                if (this.View == null)
                {
                    this.Invoke(Method);
                }
                this.View.Show();
            }
            else
            {
                this.View = (Form)RenderView;
                this.View.Show();
            }

            ((IView)this.View).Slug = Inflector.Slug(View.Name);

            AfterRender();
        }

        /// <summary>
        /// Called right upon controller construction
        /// Override this method on your child class to
        /// execute logic before loading 
        /// </summary>
        public virtual void BeforeLoad()
        {

        }

        public virtual void AfterLoad()
        {

        }

        public virtual void BeforeRender()
        {

        }

        public virtual void AfterRender()
        {

        }

    }
}
