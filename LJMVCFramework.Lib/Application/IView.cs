﻿using LJMVCFramework.Lib.Utility;
/*
 * LJMVCFramework Library: A library for building Windows Forms
 * Applications under a MVC point-of-view
 * Copyright Lemon Juice Web Apps (C) 2015  Vitor Rigoni Cavazzana
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LJMVCFramework.Lib.Application
{
    public interface IView
    {
        /// <summary>
        /// Nome da View
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Controller that owns this view.
        /// Create a property and cast it to your controller type so you'll have access to your customized controller from the view
        /// E.g.: create a propery like public UsersController UC { get { return (UsersController)OwnerController; } }
        /// </summary>
        AppController OwnerController { get; set; }

        /// <summary>
        /// Slug do nome da View.
        /// Ex: Cadastro de Usuários = cadastro-de-usuarios
        /// </summary>
        string Slug { get; set; }

        /// <summary>
        /// ViewBag para receber valores a serem passados para a view
        /// </summary>
        dynamic ViewBag { get; set; }
    }
}
